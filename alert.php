<html>
<head>
    <title>How to use sweet alert using PHP - Devnote.in</title>
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</head>
<body>

<script type="text/javascript">
    $(document).ready(function() {
        var message = "<?php Print($message); ?>";
        var title = "<?php Print($title); ?>";
        swal({
            title: title,
            text: message,
            icon: "warning",
            buttons: "Ok",
            dangerMode: true,
        });
    });
</script>
</body>
</html>