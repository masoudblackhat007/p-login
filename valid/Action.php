<?php

namespace valid;

interface Action
{
    public function existName();
    
    public function existPassword();

    public function requiredName();

    public function requirePassword();

    public function maxPassword();

    public function attemptSecurity();

    public function csrfSecurity();

    public function existSessionCSRF();

    public function existSessionDecayAttemptTime();

    public function blockSecurity();
}