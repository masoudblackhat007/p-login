<?php

namespace valid;

use Alert;
use UxWeb\SweetAlert\SweetAlert;

class HandelError implements ManagerEr
{

    private array $errors;
    use Helper;

    public function __construct()
    {
        $this->const();
    }

    public function handel(array $errors): ManagerEr{
        // TODO: Implement handel() method.
        foreach ($errors as $value) {
            foreach ($value as $element => $item) {
                foreach ($item as $key => $val) {
                    if ($val) $this->errors[$element.ucfirst("_".$key)]= $this->getConfig($element . "." . $key);
                }
            }
        }
        return $this;
    }

    /**Function index
     * @param    [viewName, data]
     *
     **/
    public function view($path, $viewLogin, $viewRedirect, $viewBlock): bool{
        // TODO: Implement view() method.
        if (!empty($this->errors)) {
            foreach ($this->errors as $key => $value) {
                $$key = $value;
                $this->action($value, $viewLogin, $viewRedirect, $viewBlock);
                exit();
            }
            return false;
        }else{
            return true;
        }
    }

    public function action($code, $viewLogin, $viewRedirect, $viewBlock){
        switch ($code){
            case EXIST_NAME:
                $this-> message('فیلد نام کاربری وجود ندارد', 'نام کاربری');
                include $viewLogin;
                break;

            case EXIST_PASSWORD:
                $this-> message('فیلد رمز عبور وجود ندارد', 'رمز عبور');
                include $viewLogin;
                break;
            case MAX_PASSWORD:
                $this-> message('رمز عبور ضعیف هست', 'رمز عبور');
                include $viewLogin;
                break;

            case REQUIRED_PASSWOPRD:
                $this-> message('رمز عبور نمی تواند خالی باشد', 'رمز عبور');
                include $viewLogin;
                break;
            case REQUIRED_NAME:
                $this-> message('نام کاربری نمی تواند خالی باشد', 'نام کاربری');
                include $viewLogin;
                break;

            case  EXIST_SESSION_CSRF:
                $this-> message('خطای ناسناخته به وجود امده', 'خطای ناشناخته');
                include  $viewRedirect;
                break;
            case BLOCK_SECURITY:
                $this-> message('به علت تلاش های مکرر شما بلاک شدید چند لحظه دیگر مجدد وارد شوید', 'مسدود');
                include  $viewBlock;
                break;

        }
    }

}
