<?php

namespace valid;

trait Helper
{

    function getConfig($index = 'base_url'){
        $config = include "configs.php";
        return $config[$index];
    }

    public function getArrayValuesRecursively(array $array): array{
//        if (!array_search('100', array_column($errors, $item))){
//                    var_dump("ok");
//                }
        $values = [];
        foreach ($array as $value) {
            if (is_array($value)) {
                $values = array_merge($values,
                    $this->getArrayValuesRecursively($value));
            } else {
                $values[] = $value;
            }
        }
        return $values;
    }

    public function get_key_in_array($s,$a) {
        if(is_array($s) && is_array($a[key($s)])) {
            return $this->get_key_in_array(reset($s), $a[key($s)]);
        } else {
            if(is_array($s)) $s = reset($s);
            return $a[$s] ?? false;
        }
    }

    public  function getValueInConfig($key1, $key2 = null){
        return empty($key2) ? $this->getConfig($key1) :$this->get_key_in_array($key2,$this->getConfig($key1));
    }

    function sessionCheck($key){

        if (session_status() === PHP_SESSION_NONE) session_start();
        if (isset($_SESSION[$key])) return true;
        return false;
    }

    function sessionStart(){
        if (session_status() === PHP_SESSION_NONE) session_start();
    }

    public function const(){
        define("EXIST_NAME", "4041");
        define("EXIST_PASSWORD", "4042");
        define("MAX_PASSWORD", "240052");
        define("REQUIRED_PASSWOPRD", "2042");
        define("REQUIRED_NAME", "2041");
        define("ATTEMPT_SECURITY", "4001");
        define("CSRF_SECURITY", "4002");
        define("EXIST_SESSION_CSRF", "4003");
        define("EXIST_SESSION_DECAY_ATTEMPT_TIME", "4004");
        define("BLOCK_SECURITY", "4005");
    }

    function redirect($router){
        header("Location: https://127.0.0.1/framework/$router");
    }

    /**Function index
     * @param    [viewName, data]
     **/
    function view_redirect($viewName, $path = '', $data = []){
        if (!empty($data)) {
            foreach ($data as $val => $value) $$val = $value;
        }
        include "views/$path/$viewName.blade.php";

    }

    public  function message($message, $title){
       include "alert.php";
    }

}