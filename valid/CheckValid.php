<?php


namespace valid;


use Factory;

class CheckValid
{
    private array $request;
    private int $max_attempt;
    private Factory $factory;
    public function __construct(array $request, int $max_attempt, Factory $factory){
        $this->request = $request;
        $this->max_attempt = $max_attempt;
        $this->factory = $factory;
    }

    public function render($path, $viewLogin, $viewRedirect, $viewBlock): array{
        $userValidation = $this->factory->user_validation($this->request);
        $errors['VALID'] = $userValidation->valid_field_name()->valid_field_password()
            ->required_name()->required_password()->max_password()->get_errors();
        $errors['security'] = $this->factory->user_security($this->request)->attempt($this->max_attempt)->check_csrf_token()->decay_user_per_time()->get_errors();
        $this->factory->handel_errors()->handel($errors)->view($path, $viewLogin, $viewRedirect, $viewBlock);
         exit();
        return $errors;


    }

    public function csrf(): string{
      return  $this->factory->user_security($this->request)->csrf_token();
    }

    public function block_user_per_time($time){
        $this->factory->user_security($this->request)->block_user_per_time($time);
    }
}