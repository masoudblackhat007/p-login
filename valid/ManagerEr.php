<?php

namespace valid;

interface ManagerEr
{
    public function handel(array $errors): ManagerEr;


    public function view($path, $viewLogin,$viewRedirect, $viewBlock);
}