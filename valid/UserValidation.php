<?php

namespace valid;

class UserValidation implements Event
{
    protected array $request;
    protected array $errors = [];
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    public function valid_field_name():Event{
        // TODO: Implement valid_field_name() method.
         $this->errors['exist']['name'] = !isset($this->request['name']) ?? false;
         return $this;
    }

    public function valid_field_password():Event{
        // TODO: Implement valid_field_password() method.
         $this->errors['exist']['password'] = !isset($this->request['password']) ?? false;
        return $this;
    }

    public function required_name():Event{
        // TODO: Implement required_name() method.
         $this->errors['required']['name'] = empty($this->request['name']) ?? false;
        return $this;
    }

    public function required_password(): Event{
        // TODO: Implement required_password() method.
        $this->errors['required']['password'] = empty($this->request['password']) ?? false;
        return $this;
    }

    public function max_password(): Event{
        // TODO: Implement max_password() method.
        if ($this->valid_field_password())
            $this->errors['max']['password'] = !(strlen($this->request['password']) >= 8) ?? false;
        return $this;
    }


    public function get_errors():array {
        // TODO: Implement get_errors() method.
        return $this->errors;

    }
}