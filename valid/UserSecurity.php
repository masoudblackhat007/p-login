<?php


namespace valid;



class UserSecurity implements Security
{

    use Helper;
    private array $request;
    private array $errors ;
    public function __construct(array $request){
        $this->request = $request;
    }

    public function attempt($attempt_max): Security{
        // TODO: Implement attempt() method.
       $this->sessionStart();
        if ($this->sessionCheck('attempt')) $_SESSION['attempt'] += 1;
        else $_SESSION['attempt'] = 0;

        if ($_SESSION['attempt'] >= 3){
            unset($_SESSION['attempt']);
            $this->errors['attempt']['security'] = true;
        }else $this->errors['attempt']['security'] = false;
        return  $this;
    }

    public function csrf_token(): string{
        // TODO: Implement csrfToken() method.
        $this->sessionStart();
        $string = base64_encode(uniqid(rand(),true));
        return $_SESSION[$_SERVER['REMOTE_ADDR']]['csrf_token'] = md5($string. time());
    }

    public function check_csrf_token(): Security{
        // TODO: Implement checkCSRFToken() method.
        $this->sessionStart();
        if ($this->exist_csrf() && $this->exist_session_csrf()){
            if ($_SESSION[$_SERVER['REMOTE_ADDR']]['csrf_token'] !== $this->request['csrf']) {
                $this->errors['csrf']['security'] = true;
                unset($_SESSION[$_SERVER['REMOTE_ADDR']]['csrf_token'],$this->request['csrf']);
            }
            else $this->errors['csrf']['security'] = false;
        }else{
            $this->errors['exist']['session_csrf'] = true;
        }

        return  $this;
    }

    public function block_user_per_time($time){
        // TODO: Implement blockUserPerTime() method.
        if (!isset($_SESSION['block_time_status'])){
            $_SESSION['decay_attempt_time'] = time() + $time;
            $_SESSION['block_time_status'] =true;
        }
    }

    public function decay_user_per_time($checkIndex = null, $unsetIndex = null): Security{
        // TODO: Implement decayUserPerTime() method.
        if ($this->exist_session_decay_attempt_time()){
            if (time() >= $_SESSION['decay_attempt_time']){
//            exit($unsetIndex);
                unset($_SESSION['attempt'],$_SESSION['block_time_status']);
                $this->errors['block']['security'] = true;
            }else  $this->errors['block']['security'] = false;
        }else{
            $this->errors['exist']['session_decay_attempt_time'] = false;
        }

        return $this;
    }


    public function get_errors():array{
        // TODO: Implement get_errors() method.
        return $this->errors;
    }

    private function exist_session_csrf(): bool {
        // TODO: Implement exist_session() method.
        if (isset($_SESSION[$_SERVER['REMOTE_ADDR']]['csrf_token'])){
            return true;
        }
        return false;

    }

    public function exist_session_decay_attempt_time(): bool{
        if (isset($_SESSION['decay_attempt_time'])){
            return true;
        }
        return false;
    }

    private function exist_csrf(): bool{
        // TODO: Implement exist_csrf() method.
        if (isset($this->request['csrf'])){
            return true;
        }
        return false;
    }
}