<?php

use valid\Event;
use valid\ManagerEr;
use valid\Security;

interface Factory
{
    public function user_validation(array $request): Event;

    public function user_security(array $request): Security;

    public function handel_errors():ManagerEr;
}