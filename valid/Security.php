<?php
namespace valid;
interface Security
{

    public function attempt($attempt_max):Security;

    public function csrf_token():string;

    public function check_csrf_token(): Security;

    public function block_user_per_time($time);

    public function decay_user_per_time(): Security;

    public function get_errors():array;

}